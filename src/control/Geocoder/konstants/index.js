export const VARS = {
  containerId: 'gcd-container',
  buttonControlId: 'gcd-button-control',
  inputQueryId: 'gcd-input-query',
  inputResetId: 'gcd-input-reset',
  cssClasses: {
    namespace: 'ol-geocoder',
    spin: 'gcd-pseudo-rotate',
    hidden: 'gcd-hidden',
    address: 'gcd-address',
    country: 'gcd-country',
    city: 'gcd-city',
    road: 'gcd-road',
    olControl: 'ol-control',
    glass: {
      container: 'gcd-gl-container',
      control: 'gcd-gl-control',
      button: 'gcd-gl-btn',
      input: 'gcd-gl-input',
      expanded: 'gcd-gl-expanded',
      reset: 'gcd-gl-reset',
      result: 'gcd-gl-result',
    },
    inputText: {
      container: 'gcd-txt-container',
      control: 'gcd-txt-control',
      input: 'gcd-txt-input',
      reset: 'gcd-txt-reset',
      icon: 'gcd-txt-glass',
      result: 'gcd-txt-result',
    },
  },
};

export const EVENT_TYPE = {
  ADDRESSCHOSEN: 'addresschosen',
};

export const CONTROL_TYPE = {
  NOMINATIM: 'nominatim',
  REVERSE: 'reverse',
};

export const TARGET_TYPE = {
  GLASS: 'glass-button',
  INPUT: 'text-input',
};

export const FEATURE_SRC = '//cdn.rawgit.com/jonataswalker/map-utils/master/images/marker.png';

export const PROVIDERS = {
  OSM: 'osm',
  MAPQUEST: 'mapquest',
  PHOTON: 'photon',
  BING: 'bing',
  OPENCAGE: 'opencage',
};

export const DEFAULT_OPTIONS = {
  provider: PROVIDERS.OSM,
  placeholder: 'Search for an address',
  featureStyle: null,
  targetType: TARGET_TYPE.GLASS,
  lang: 'en-US',
  limit: 5,
  keepOpen: false,
  preventDefault: false,
  autoComplete: false,
  autoCompleteMinLength: 2,
  autoCompleteTimeout: 200,
  debug: false,
};
