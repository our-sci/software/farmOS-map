import GeoJSON from 'ol/format/GeoJSON';
import unkinkPolygon from '@turf/unkink-polygon';

/**
 * Unkink the polygon.
 * @param {ol.Feature} feature The polygon feature to unkink.
 * @return {ol.Feature} The polygon feature that has the biggest area.
 */
export function unkinkPolygonFeature(feature) {
  const type = feature.getGeometry().getType();
  if (type !== 'Polygon') {
    return feature;
  }

  const geoJson = new GeoJSON();
  const unkinkedGeoJson = unkinkPolygon(geoJson.writeFeatureObject(feature));
  const featureCollection = geoJson.readFeatures(unkinkedGeoJson);
  const [polygonFeature] = featureCollection.sort(
    (a, b) => b.getGeometry().getArea() - a.getGeometry().getArea(),
  );

  return polygonFeature || feature;
}
